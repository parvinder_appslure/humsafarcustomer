import React from 'react';
import {StyleSheet} from 'react-native';
import StackNavigator from './screens/navigator/StackNavigator';

const App = () => {
  return <StackNavigator />;
};

export default App;
