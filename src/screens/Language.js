import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {BottomButton, Header, MainView} from '../Custom/CustomView';

const Language = ({navigation}) => {
  return (
    <MainView>
      <Header onPress={() => navigation.goBack()} title="Change Language" />
      <Text style={styles.headerText}>Select Your Language</Text>

      <View style={styles.topView}>
        <View style={styles.languageSelected}>
          <Image
            source={require('../assets/language.png')}
            style={styles.headerImage}
          />
          <Text style={styles.headerText1}>English</Text>
        </View>
        <View style={styles.languageUnselected}>
          <Image
            source={require('../assets/language.png')}
            style={styles.headerImage}
          />
          <Text style={styles.headerText1}>हिंदी</Text>
        </View>
      </View>
      <BottomButton
        onPress={() => navigation.navigate('home')}
        bottomtitle="CONTINUE"
      />
    </MainView>
  );
};

export default Language;

const styles = StyleSheet.create({
  headerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
    margin: 20,
  },
  topView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  languageSelected: {
    backgroundColor: '#ED6E1E',
    padding: 30,
    borderRadius: 20,
  },
  languageUnselected: {
    backgroundColor: '#00000033',
    padding: 30,
    borderRadius: 20,
  },
  headerImage: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  headerText1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 10,
  },
});
