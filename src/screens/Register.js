import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BottomButton, BottomView, MainImage} from '../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {CustomInputField} from './Login';

const Register = ({navigation}) => {
  const [state, setState] = useState({
    userType: '',
    companyName: '',
    fullName: '',
    email: '',
    mobile: '',
    password: '',
    confirmPassword: '',
    referal: '',
  });

  return (
    <KeyboardAwareScrollView>
      <MainImage>
        <BottomView>
          <Text style={styles.topText}>Register</Text>
          <Text style={styles.topText_1}>Enter your basic details</Text>

          <CustomInputField
            tfSource={require('../assets/man-user.png')}
            label={'Select User Type'}
            value={state.userType}
            onChangeText={userType => setState({...state, userType})}
          />

          <CustomInputField
            tfSource={require('../assets/building.png')}
            label={'Company Name'}
            value={state.companyName}
            onChangeText={companyName => setState({...state, companyName})}
          />

          <CustomInputField
            tfSource={require('../assets/man-user.png')}
            label={'Full Name*'}
            value={state.fullName}
            onChangeText={fullName => setState({...state, fullName})}
          />

          <CustomInputField
            tfSource={require('../assets/email.png')}
            label={'Email Address'}
            value={state.email}
            onChangeText={email => setState({...state, email})}
          />

          <CustomInputField
            tfSource={require('../assets/phone.png')}
            label={`Mobile Number`}
            value={state.mobile}
            onChangeText={mobile => setState({...state, mobile})}
          />

          <CustomInputField
            tfSource={require('../assets/lock.png')}
            label={'Password*'}
            value={state.password}
            onChangeText={password => setState({...state, password})}
          />

          <CustomInputField
            tfSource={require('../assets/lock.png')}
            label={'Confirm Password*'}
            value={state.confirmPassword}
            onChangeText={confirmPassword =>
              setState({...state, confirmPassword})
            }
          />

          <CustomInputField
            tfSource={require('../assets/referal.png')}
            label={'Referral Code (optional)'}
            value={state.referal}
            onChangeText={referal => setState({...state, referal})}
          />
          <BottomButton
            onPress={() => navigation.navigate('Otp')}
            bottomtitle="SIGN UP"
          />

          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.bottomText}>
              Already have an account?
              <Text style={styles.bottomText_1}>{` Login Now`}</Text>
            </Text>
          </TouchableOpacity>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F9F9F9',
    backgroundColor: '#F9F9F9',
    borderRadius: 20,
    fontSize: 35,
    color: '#000000',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#ED6E1E',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#E7E7E7',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
});
