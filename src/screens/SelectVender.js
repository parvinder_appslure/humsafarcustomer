import React from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {Header} from '../Custom/CustomView';

const SelectVender = ({navigation}) => {
  const NotifyList = props => (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => navigation.navigate('Payment')}
      style={styles.nlBg}>
      <View style={{flex: 1, marginRight: 5}}>
        <Text style={styles.listText}>{props.message}</Text>
        <Text style={styles.listText_1}>{props.date}</Text>
      </View>
      <View>
        <Text style={styles.listText}>{props.distance}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <ScrollView>
      <View styles={styles.bg}>
        <Header onPress={() => navigation.goBack()} title={'SelectVender'} />
        <NotifyList
          message={'Bharat Petrol Pump'}
          date="Rohini Sector-8, South Avenue Apartment, New Delhi-110080"
          distance="3 Km"
        />
        <NotifyList
          message={'HP Petrol Pump'}
          date="Rohini Sector-8, South Avenue Apartment, New Delhi-110080"
          distance="3 Km"
        />
        <NotifyList
          message={'Hindustan Petrol Pump'}
          date="Noida Sector-63, South Avenue Apartment, New Delhi-110021"
          distance="3 Km"
        />
        <NotifyList
          message={'Indian Oil Petrol Pump'}
          date="Dwarka Sector-17, South Avenue Apartment, New Delhi-110032"
          distance="3 Km"
        />
        <NotifyList
          message={'BPCL Petrol Pump'}
          date="Dwarka Sector-17, South Avenue Apartment, New Delhi-110032"
          distance="3 Km"
        />
        <NotifyList
          message={'Indian Oil Petrol Pump'}
          date="Dwarka Sector-17, South Avenue Apartment, New Delhi-110032"
          distance="3 Km"
        />
        <NotifyList
          message={'Indian Oil Petrol Pump'}
          date="Dwarka Sector-17, South Avenue Apartment, New Delhi-110032"
          distance="3 Km"
        />
      </View>
    </ScrollView>
  );
};

export default SelectVender;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: '#F7F7F7',
    flex: 1,
  },
  nlBg: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    padding: 10,
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  listText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000521',
  },
  listText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#69707F',
    lineHeight: 20,
  },
});
