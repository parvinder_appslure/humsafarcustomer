import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {MainView} from '../Custom/CustomView';

const Security = ({navigation}) => {
  const HistoryList = props => (
    <View style={styles.hlBackground}>
      <View style={styles.topList}>
        <View style={styles.line} />
        <View style={{flex: 1}}>
          <View style={styles.nfRow}>
            <Text style={styles.listText}>Jerry Can Security (1)</Text>
            <Text style={[styles.listText, {color: '#FF0000'}]}>
              {props.amount}
            </Text>
          </View>

          <View style={styles.nfRow}>
            <Text style={styles.listText_1}>{props.date}</Text>
            <Text style={styles.listText_1}>{props.id}</Text>
          </View>
        </View>
      </View>
    </View>
  );
  return (
    <MainView>
      <ScrollView>
        <View style={styles.topView}>
          <Text style={styles.topText}>SECURITY AMOUNT</Text>
          <Text style={styles.topText_1}>₹1500.00</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('WithdrawAmount')}>
            <Text style={styles.topText_2}>Withdraw Amount</Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.middleText}>Transaction History</Text>
        <HistoryList
          amount="-₹10000"
          date="26 July, 02:19 PM"
          id="ID: #45683 "
        />
        <HistoryList
          amount="-₹10000"
          date={'26 July, 02:19 PM'}
          id="ID: #45683 "
        />
        <HistoryList
          amount="-₹10000"
          date={'26 July, 02:19 PM'}
          id="ID: #45683 "
        />
        <HistoryList
          amount="-₹10000"
          date={'26 July, 02:19 PM'}
          id="ID: #45683 "
        />
        <HistoryList
          amount="-₹10000"
          date={'26 July, 02:19 PM'}
          id="ID: #45683 "
        />
        <HistoryList
          amount="-₹10000"
          date={'26 July, 02:19 PM'}
          id="ID: #45683 "
        />
      </ScrollView>
    </MainView>
  );
};

export default Security;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },

  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },

  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#000000',
    marginHorizontal: 20,
  },

  hlBackground: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  line: {
    padding: 3,
    backgroundColor: '#ED6E1E',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },

  topList: {
    flexDirection: 'row',
  },

  listText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
  },
  listText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#83878E',
  },
  nfRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
});
