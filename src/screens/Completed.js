import React, {useState} from 'react';
import {FlatList, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Dash from 'react-native-dash';
import {StatusBarLight} from '../Custom/CustomStatusBar';

const Completed = ({navigation}) => {
  const person = [
    {
      key: 1,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 2,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 3,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 4,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 5,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 6,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Completed',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
  ];
  return (
    <View>
      <StatusBarLight />
      <FlatList
        data={person}
        numColumns={1}
        renderItem={({item}) => {
          return (
            <>
              <View style={styles.bg}>
                <View style={styles.ftview}>
                  <Text style={styles.ftText}>{item.order}</Text>
                  <Text style={styles.ftText1}>{item.date}</Text>
                </View>

                <DashLine />

                <Text style={styles.ftText2}>
                  Status:
                  <Text style={[styles.ftText2, {color: '#00C327'}]}>
                    {item.ongoing}
                  </Text>
                </Text>

                <View style={styles.ftview_1}>
                  <View>
                    <Text style={styles.ftText_2}>Name</Text>
                    <Text style={styles.ftText_2}>Address</Text>
                    <Text style={styles.ftText_2}>Date & Time</Text>
                    <Text style={styles.ftText_2}> Diesel Price</Text>
                    <Text style={styles.ftText_2}>Jerry Can</Text>
                    <Text style={styles.ftText_2}>Payment Mode</Text>
                    <Text style={styles.ftText_2}>OTP</Text>
                  </View>

                  <View>
                    <Text style={styles.ftText_3}>{item.name}</Text>
                    <Text style={styles.ftText_3}>{item.address}</Text>
                    <Text style={styles.ftText_3}>{item.dateTime}</Text>
                    <Text style={styles.ftText_3}>{item.dieselPrice}</Text>
                    <Text style={styles.ftText_3}>{item.jerryCan}</Text>
                    <Text style={styles.ftText_3}>{item.paymenyMethod}</Text>
                    <Text style={[styles.ftText_3, {color: '#ED6E1E'}]}>
                      {item.otp}
                    </Text>
                  </View>
                </View>

                <DashLine />
                <View style={[styles.ftview_1, {marginTop: 0}]}>
                  <Text style={styles.ftText}>
                    Total:
                    <Text style={{color: '#F87B00'}}>{item.total}</Text>
                  </Text>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('home3')}>
                    <Text style={[styles.ftText, {color: '#F87B00'}]}>
                      Repeat Order
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </>
          );
        }}
      />
    </View>
  );
};

export default Completed;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 10,
    top: 10,
    borderRadius: 10,
    marginHorizontal: 20,
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#9F9F9F',
  },
  dash: {
    borderColor: '#6F6F7B',
    marginHorizontal: 20,
    borderStyle: 'dotted',
    borderWidth: 1,
    marginTop: 10,
  },
  ftText2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#9F9F9F',
    marginHorizontal: 10,
  },

  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
  },
  ftText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 3,
  },
  ftText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#83878E',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#000000',
    alignSelf: 'flex-end',
  },
  ftText_4: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#000000',
    alignSelf: 'flex-end',
  },
  payButton: {
    padding: 5,
    borderRadius: 4,
    backgroundColor: '#ED6E1E',
  },
  payText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    margin: 10,
  },
});

export const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
