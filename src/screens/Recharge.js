import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  FlatList,
} from 'react-native';

import {RadioButton} from 'react-native-paper';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, Header} from '../Custom/CustomView';

const Recharge = ({navigation}) => {
  // const [checked, setChecked] = React.useState('first');
  const [itemState, setItemState] = useState(0);
  const [state, setState] = useState({
    first: '',
    price: '',
  });

  const Amount = [
    {
      key: 1,
      rupees: '₹ 200',
    },
    {
      key: 2,
      rupees: '₹ 500',
    },
    {
      key: 3,
      rupees: '₹ 1000',
    },
    {
      key: 4,
      rupees: '₹ 2000',
    },
    {
      key: 5,
      rupees: '₹ 5000',
    },
    {
      key: 6,
      rupees: '₹ 100000',
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <StatusBarDark />
        <Header onPress={() => navigation.goBack()} title="Recharge" />
        <View style={styles.topView}>
          <Text style={styles.topText}>AVAILABLE BALANCE</Text>
          <Text style={styles.topText_1}>₹25000.00</Text>
        </View>

        <View style={styles.middleView}>
          <View style={styles.nintyPercnetView}>
            <Text style={styles.topText1}>Enter Amount</Text>
            <TextInput
              underlineColorAndroid="#00000030"
              keyboardType="name-phone-pad"
            />
            <Text style={styles.middleText}>
              Choose from the available wallet balance.
            </Text>

            <FlatList
              numColumns={3}
              data={Amount}
              keyExtractor={item => item.toString()}
              //   contentContainerStyle={styles.flatList}
              renderItem={({item}) => (
                <TouchableOpacity
                  // onPress={() => setItemState(+item)}
                  style={styles.boxView}>
                  <Text style={styles.middleText_1}>{item.rupees}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>

        <View style={styles.middleView}>
          <Text style={styles.bottomtext}>Payment Options</Text>
          <View style={styles.bottomView} />
          <View style={styles.bottomView_1}>
            <RadioButton
              value="first"
              status={state === 'first' ? 'checked' : 'unchecked'}
              onPress={() => setState('first')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            />
            <Text style={styles.bottomtext}>
              Debit / Credit Card / Netbanking
            </Text>

            <Image
              source={require('../assets/card.png')}
              style={styles.bottomimage}
            />
          </View>
          <View style={styles.bottomView_2} />

          <View style={styles.bottomView_1}>
            <RadioButton
              value="second"
              status={state === 'second' ? 'checked' : 'unchecked'}
              onPress={() => setState('second')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            />
            <Text style={styles.bottomtext}>Pay with Paytm</Text>

            <Image
              source={require('../assets/paytm.png')}
              style={styles.walletimage}
            />
          </View>
        </View>

        <View style={styles.bt}>
          <BottomButton
            onPress={() => navigation.navigate('')}
            bottomtitle="PROCEED TO PAY"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  nintyPercnetView: {
    marginHorizontal: 20,
  },
  flatList: {
    flexGrow: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
  },
  topView: {
    padding: 20,
    backgroundColor: '#31B9EB',
    marginHorizontal: 20,
    borderRadius: 20,
    margin: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  middleView: {
    backgroundColor: '#FFFFFF',
    marginTop: 20,
    paddingBottom: 20,
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#69707F',
    marginLeft: 8,
    marginTop: 20,
    marginBottom: 10,
  },
  middleText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#83878E',
    textAlign: 'center',
  },
  topText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#69707F',
    marginTop: 10,
  },
  boxView: {
    margin: 5,
    borderRadius: 10,
    borderColor: '#CED4E2',
    borderWidth: 1,
    padding: 10,
    width: '30%',
  },
  bottomtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 10,
    marginHorizontal: 20,
  },
  bottomView: {
    borderColor: '#0000000d',
    borderWidth: 0.5,
    marginTop: 10,
  },
  bottomView_1: {
    flexDirection: 'row',
    marginTop: 10,
    marginHorizontal: 20,
  },
  bottomimage: {
    width: 24,
    height: 16,
    resizeMode: 'contain',
    marginTop: 10,
    marginLeft: 'auto',
  },
  bottomView_2: {
    width: '90%',
    borderColor: '#000000',
    opacity: 0.05,
    borderWidth: 0.6,
    marginTop: 10,
    alignSelf: 'center',
  },
  walletimage: {
    width: 40,
    height: 13,
    resizeMode: 'contain',
    marginTop: 10,
    marginLeft: 'auto',
  },
  bt: {
    marginHorizontal: 15,
    marginTop: 50,
  },
});

export default Recharge;
