import React from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton} from '../Custom/CustomView';

const home3 = ({navigation}) => {
  return (
    <ScrollView>
      <StatusBarDark />
      <View style={styles.topView}>
        <View style={styles.header}>
          <TouchableOpacity onPress={navigation.openDrawer}>
            <Image
              source={require('../assets/menu.png')}
              style={styles.headerImage}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Image
              source={require('../assets/notification.png')}
              style={styles.headerImage_1}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Text style={styles.headerText}>Order Detail</Text>
      <View style={styles.subview}>
        <View style={styles.subview_1}>
          <Text style={styles.subtext}>Current Diesel Price</Text>
          <Text style={styles.subtext}>89.91/litre</Text>
        </View>
      </View>

      <Text style={styles.headerText_1}>Safar Can quantity (in litres)</Text>
      <TextInput style={styles.inputfield} />
      <View style={[styles.subview_1, {marginTop: 20}]}>
        <Text style={[styles.subtext, {color: '#000000', fontSize: 16}]}>
          Date
        </Text>
        <Text style={[styles.subtext, {color: '#000000', fontSize: 16}]}>
          Time
        </Text>
      </View>

      <BottomButton
        onPress={() => navigation.navigate('Payment')}
        bottomtitle="CONTINUE"
      />
    </ScrollView>
  );
};

export default home3;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    padding: 10,
  },
  headerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
    alignSelf: 'center',
    marginTop: 20,
  },
  headerImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  headerImage_1: {
    width: 18,
    height: 20,
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 10,
    justifyContent: 'space-between',
  },
  subview: {
    backgroundColor: '#FFFFFF',
    elevation: 5,
    padding: 10,
    marginTop: 10,
    marginHorizontal: 20,
  },
  subview_1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  subtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#ED6E1E',
  },
  headerText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 20,
    marginHorizontal: 20,
  },
  inputfield: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
    elevation: 5,
  },
});
