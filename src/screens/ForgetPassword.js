import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {BottomButton, BottomView, MainImage} from '../Custom/CustomView';
import {CustomInputField} from './Login';
// import {CustomInputField} from './Login';

const ForgetPassword = ({navigation}) => {
  return (
    <KeyboardAwareScrollView>
      <MainImage>
        <BottomView>
          <Text style={styles.topText}>Forgot Password</Text>
          <Text style={styles.topText_1}>
            Enter your Email Address to reset your password
          </Text>

          <CustomInputField
            tfSource={require('../assets/phone.png')}
            label={'Mobile Number'}
          />
          <BottomButton
            onPress={() => navigation.navigate('Otp')}
            bottomtitle="RESET PASSWORD"
            //   marginTop={100}
          />
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default ForgetPassword;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    // marginHorizontal: 10,
    marginLeft: 10,
    marginTop: 4,
  },
});
