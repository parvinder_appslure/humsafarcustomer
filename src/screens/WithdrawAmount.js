import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, Header, MainView} from '../Custom/CustomView';

const WithdrawAmount = () => {
  const CustomTextInput = props => (
    <TextInput
      placeholder="Account Name"
      underlineColorAndroid="#F1F3F8"
      style={styles.textInput}
      {...props}
    />
  );

  return (
    <MainView>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title="Withdraw Amount" />

      <CustomTextInput placeholder="Account Name" />
      <CustomTextInput placeholder="Bank Name" />
      <CustomTextInput placeholder="Account No." />
      <CustomTextInput placeholder="IFSC Code" />

      <BottomButton
        onPress={() => navigation.navigate('Otp')}
        bottomtitle="SEND REQUEST"
      />
    </MainView>
  );
};

export default WithdrawAmount;

const styles = StyleSheet.create({
  textInput: {
    marginHorizontal: 20,
    fontSize: 16,
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    marginTop: 10,
  },
});
