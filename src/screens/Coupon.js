import React from 'react';
import {
  FlatList,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {StatusBarDark, StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const Coupon = ({navigation}) => {
  const coupon = [
    {
      key: 1,
      discount: '20%',
    },
    {
      key: 2,
      discount: '20%',
    },
    {
      key: 3,
      discount: '20%',
    },
    {
      key: 4,
      discount: '20%',
    },
    {
      key: 5,
      discount: '20%',
    },
    {
      key: 6,
      discount: '20%',
    },
    {
      key: 7,
      discount: '20%',
    },
  ];

  const Discount = props => (
    <View>
      <Text style={styles.ftText_2}>
        Use Code
        <Text style={{color: '#03CC15'}}>{props.promo} </Text>& get
        <Text>{props.offer}</Text>
        off on orders above
        <Text>₹149.</Text>
      </Text>
    </View>
  );

  return (
    <ScrollView>
      <View>
        <StatusBarDark />
        <Header onPress={() => navigation.goBack()} title="Apply Coupon" />
        <FlatList
          data={coupon}
          keyExtractor={item => item.key}
          renderItem={({item}) => {
            return (
              <>
                <ImageBackground
                  source={require('../assets/box.png')}
                  style={styles.ftIg}>
                  <View style={styles.ftTop}>
                    <View style={{marginLeft: 10}}>
                      <Text style={styles.ftText}>{item.discount}</Text>
                      <Text style={styles.ftText_1}>Off</Text>
                    </View>
                    <View style={styles.ftview}>
                      <Discount promo=" TRYNEW" offer=" 20% " />

                      <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={styles.ftview_1}>
                        <Text style={styles.ftText_3}>APPLY</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ImageBackground>
              </>
            );
          }}
        />
        <View></View>
      </View>
    </ScrollView>
  );
};

export default Coupon;

const styles = StyleSheet.create({
  ftIg: {
    width: 319,
    height: 114,
    resizeMode: 'contain',
    marginTop: 20,
    alignSelf: 'center',
    marginBottom: 20,
  },
  ftTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: 10,
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 22,
    color: '#ED6E1E',
    marginTop: 20,
  },
  ftText_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#ED6E1E',
    textAlign: 'center',
  },
  ftview: {
    width: '65%',
  },
  ftText_2: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 13,
    color: '#080040',
    marginTop: 10,
    lineHeight: 20,
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#ED6E1E',
  },
  ftview_1: {
    backgroundColor: '#ED6E1E25',
    padding: 5,
    // textAlign: 'center',
    width: '35%',
    alignItems: 'center',
    marginTop: 10,
    borderRadius: 4,
  },
});
