import React, {useEffect} from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextField} from 'react-native-material-textfield';
import {BottomButton} from '../Custom/CustomView';

const {height} = Dimensions.get('window');

export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <Image source={props.tfSource} style={styles.tfImage} />
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        label="Phone number"
        tintColor="#8D92A3"
        {...props}
      />
    </View>
    <View style={styles.line} />
  </>
);

const Login = ({navigation}) => {
  return (
    <KeyboardAwareScrollView>
      <ImageBackground
        source={require('../assets/background.png')}
        style={styles.bg}>
        <View style={styles.BottomView}>
          <Text style={styles.topText}>Login</Text>
          <Text style={styles.topText_1}>
            We will send you confirmation code
          </Text>

          <CustomInputField
            tfSource={require('../assets/phone.png')}
            label={'Mobile'}
          />
          <CustomInputField
            tfSource={require('../assets/lock.png')}
            label={'Password'}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('ForgetPassword')}>
            <Text style={styles.bottomtext}>Forgot password?</Text>
          </TouchableOpacity>

          <BottomButton
            onPress={() => navigation.navigate('Location2')}
            bottomtitle="SIGN IN"
          />

          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.bottomtext1}>
              Don’t have an account?
              <Text style={{color: '#ED6E1E'}}>{` Signup Now`}</Text>
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </KeyboardAwareScrollView>
  );
};
export default Login;

const styles = StyleSheet.create({
  bg: {
    flex: 1,
  },
  BottomView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 20,
    marginTop: height / 2,
    paddingTop: 20,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 15,
    fontWeight: '400',
    color: '#8D92A3',
    marginHorizontal: 10,
    marginTop: 4,
  },
  tfStyle: {
    // marginLeft: 30,
    width: '85%',
    marginHorizontal: 10,
  },
  tfImage: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginTop: 15,
    marginLeft: 10,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    width: '95%',
    borderColor: '#F1F3F8',
    borderWidth: 0.5,
    marginHorizontal: 10,
    // borderColor:''
  },
  bottomtext: {
    fontFamily: 'Avenir-Roman',
    fontSize: 12,
    fontWeight: '400',
    color: '#8D92A3',
    marginTop: 10,
    alignSelf: 'flex-end',
    marginHorizontal: 5,
  },
  bottomtext1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    marginTop: 5,
    alignSelf: 'center',
    marginBottom: 10,
  },
});
