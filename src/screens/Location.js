import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, MainView} from '../Custom/CustomView';

const Location = () => {
  return (
    <MainView>
      <StatusBarDark />
      <View style={styles.topView}>
        <View style={styles.header}>
          <Image
            source={require('../assets/location.png')}
            style={styles.headerImage}
          />
          <Text style={styles.headerText}>Pearls Omaxe, Netaji…</Text>
        </View>
      </View>

      <BottomButton
        onPress={() => navigation.navigate('Otp')}
        bottomtitle="SIGN UP"
      />
    </MainView>
    // <View style>
    //
    // </View>
  );
};

export default Location;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    padding: 10,
    // marginTop: StatusBar.currentHeight +
  },
  headerText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 10,
  },
  headerImage: {
    width: 12,
    height: 18,
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 20,
  },
});
