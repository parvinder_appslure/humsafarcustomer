import React from 'react';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Header} from '../../Custom/CustomView';
import Upcoming from '../Upcoming';
import Completed from '../Completed';
import Cancelled from '../Cancelled';

const Tab = createMaterialTopTabNavigator();

const TopTabNavigation = ({navigation}) => {
  return (
    <>
      <Header onPress={navigation.goBack} title={'My Order'} />
      <Tab.Navigator
        // initialRouteName="Explore"

        tabBarOptions={{
          keyboardHidesTabBar: true,
          //   scrollEnabled: true,
          inactiveTintColor: '#000521',
          activeTintColor: '#ED6E1E',
          labelStyle: {
            fontFamily: 'Avenir-Medium',
            fontWeight: 'bold',
            fontSize: 14,
          },
          style: {
            backgroundColor: '#FFFFFF',
          },
          indicatorStyle: {
            backgroundColor: '#ED6E1E',
          },
        }}>
        <Tab.Screen
          name="Upcoming"
          component={Upcoming}
          options={{
            headerShown: false,
            tabBarLabel: 'Upcoming',
          }}
        />

        <Tab.Screen
          name="Completed"
          component={Completed}
          options={{
            tabBarLabel: 'COMPLETED',
          }}
        />
        <Tab.Screen
          name="Cancelled"
          component={Cancelled}
          options={{
            tabBarLabel: 'CANCELED',
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default TopTabNavigation;
