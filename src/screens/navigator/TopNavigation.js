import React from 'react';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Header} from '../../Custom/CustomView';
import Wallet from '../Wallet';
import Cashback from '../Cashback';
import Security from '../Security';

const Tab = createMaterialTopTabNavigator();

const TopNavigation = ({navigation}) => {
  return (
    <>
      <Header onPress={navigation.goBack} title={'Wallet'} />
      <Tab.Navigator
        // initialRouteName="Explore"

        tabBarOptions={{
          keyboardHidesTabBar: true,
          //   scrollEnabled: true,
          inactiveTintColor: '#000521',
          activeTintColor: '#ED6E1E',
          labelStyle: {
            fontFamily: 'Avenir-Medium',
            fontWeight: 'bold',
            fontSize: 14,
          },
          style: {
            backgroundColor: '#FFFFFF',
          },
          indicatorStyle: {
            backgroundColor: '#ED6E1E',
          },
        }}>
        <Tab.Screen
          name="Wallet"
          component={Wallet}
          options={{
            headerShown: false,
            tabBarLabel: 'WALLET',
          }}
        />

        <Tab.Screen
          name="Cashback"
          component={Cashback}
          options={{
            tabBarLabel: 'CASHBACK',
          }}
        />
        <Tab.Screen
          name="Security"
          component={Security}
          options={{
            tabBarLabel: 'SECURITY',
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default TopNavigation;
