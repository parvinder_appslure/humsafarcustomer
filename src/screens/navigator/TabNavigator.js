import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import Home from '../Home';
import Search from '../Search';
import MyProfile from '../MyProfile';

const iconPath = {
  h: require('../../Image/home.png'),
  ha: require('../../Image/home-1.png'),
  s: require('../../Image/search.png'),
  sa: require('../../Image/search-1.png'),
  p: require('../../Image/avatar.png'),
  pr: require('../../Image/avatar-1.png'),
};

const Tab = createBottomTabNavigator();
const TabIcon = source => <Image source={source} style={styles.tabIcon} />;

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelStyle: {
          paddingBottom: 2,
          fontSize: 10,
          fontFamily: 'Avenir-Roman',
          fontWeight: '400',
        },
        activeTintColor: '#008080',
        activeBackgroundColor: '#FFFFFF',
        inactiveBackgroundColor: '#FFFFFF',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.h : iconPath.ha),
        }}
      />

      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.s : iconPath.sa),
        }}
      />

      <Tab.Screen
        name="MyProfile"
        component={MyProfile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.p : iconPath.pr),
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});
