import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawerContent from '../CustomDrawerContent';
import TabNavigator from './TabNavigator';

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => (
  <Drawer.Navigator
    initialRouteName="TabNavigator"
    drawerStyle={{width: '70%'}}
    drawerContent={props => <CustomDrawerContent {...props} />}>
    <Drawer.Screen name="TabNavigator" component={TabNavigator} />
  </Drawer.Navigator>
);
export default DrawerNavigator;
