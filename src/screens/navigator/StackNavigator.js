import React, {useRef, useEffect} from 'react';

import {CardStyleInterpolators} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
// import {createDrawerNavigator} from '@react-navigation/drawer';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Splash from '../Splash';
import ForgetPassword from '../ForgetPassword';
import Otp from '../Otp';
import Login from '../Login';
import Location from '../Location';
import Register from '../Register';
import Location2 from '../Location2';
import home3 from '../home3';
import Payment from '../Payment';
import Coupon from '../Coupon';
import PaymentSuccessfully from '../PaymentSuccessfully';
import TopTabNavigation from './TopTabNavigation';
import Notification from '../Notification';
import Home1 from '../Home1';
import home from '../home';
import CustomDrawerContent from '../CustomDrawerContent';
import SelectVender from '../SelectVender';
import Language from '../Language';
import SavedAddress from '../SavedAddress';
import ReturnJerryCan from '../ReturnJerryCan';
import TopNavigation from './TopNavigation';
import Recharge from '../Recharge';
import WithdrawAmount from '../WithdrawAmount';

const Drawer = createDrawerNavigator();
const MyDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="home3"
      drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="home3" component={home3} />
    </Drawer.Navigator>
  );
};

const Stack = createNativeStackNavigator();
const StackNavigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => console.log(navigationRef.current.getCurrentRoute().name)}
      onStateChange={() =>
        console.log(`Screen :: `, navigationRef.current.getCurrentRoute().name)
      }>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="ForgetPassword"
          component={ForgetPassword}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Location"
          component={Location}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Location2"
          component={Location2}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Payment"
          component={Payment}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Coupon"
          component={Coupon}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentSuccessfully"
          component={PaymentSuccessfully}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TopTabNavigation"
          component={TopTabNavigation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Home1"
          component={Home1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyDrawer"
          component={MyDrawer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="home"
          component={home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectVender"
          component={SelectVender}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Language"
          component={Language}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SavedAddress"
          component={SavedAddress}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ReturnJerryCan"
          component={ReturnJerryCan}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TopNavigation"
          component={TopNavigation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Recharge"
          component={Recharge}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="WithdrawAmount"
          component={WithdrawAmount}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;
