import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import CustomModal from '../Custom/CustomModal';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const {height, width} = Dimensions.get('window');
const ReturnJerryCan = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Delivery Options:</Text>
          </View>
          <View style={styles.mdmiddleView}>
            <View style={styles.mdmiddle}>
              <Image
                source={require('../assets/truck.png')}
                style={styles.mdImage}
              />
              <Text style={styles.mdText}>Browser</Text>
            </View>

            <View style={styles.mdmiddle_1}>
              <Image
                source={require('../assets/can.png')}
                style={styles.mdImage_1}
              />
              <Text style={styles.mdText}>Safar 20 (Jerry Can)</Text>
            </View>
          </View>

          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.mdbottomView}
            onPress={() => {
              navigation.navigate('');
              setModalOpen(false);
            }}>
            <Text style={styles.mdBottomText}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ReturnJerryCan;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#31B9EB',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  mdmiddle: {
    backgroundColor: '#ED6E1E',
    padding: 10,
    width: width / 3,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage: {
    width: 95,
    height: 42,
    resizeMode: 'contain',
    marginTop: 10,
  },
  mdText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 5,
  },
  mdmiddle_1: {
    backgroundColor: '#00000030',
    padding: 10,
    width: width / 2.5,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage_1: {
    width: 40,
    height: 48,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 10,
  },
  mdmiddleView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 15,
  },
  mdbottomView: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
