import React from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Header} from '../Custom/CustomView';

const Notification = ({navigation}) => {
  const NotifyList = props => (
    <View style={styles.nlBg}>
      <View style={styles.topList}>
        <View style={styles.line} />
        <Image
          source={require('../assets/bell.png')}
          style={styles.listImage}
        />
        <View>
          <Text style={styles.listText}>{props.message}</Text>
          <Text style={styles.listText_1}>{props.date}</Text>
        </View>
      </View>
    </View>
  );

  return (
    <ScrollView>
      <View styles={styles.bg}>
        <Header onPress={() => navigation.goBack()} title={'Notification'} />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
        <NotifyList
          message={'Your Diesel Package Delivered Successfully.'}
          date="04 Aug 2021"
        />
      </View>
    </ScrollView>
  );
};

export default Notification;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: '#F7F7F7',
    flex: 1,
  },
  nlBg: {
    backgroundColor: '#FFFFFF',
    marginTop: 20,
    marginHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    marginBottom: 10,
  },
  line: {
    width: '2%',
    backgroundColor: '#ED6E1E',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  topList: {
    flexDirection: 'row',
  },
  listImage: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    margin: 10,
  },
  listText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000521',
    marginHorizontal: 10,
    marginTop: 10,
  },
  listText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#ED6E1E',
    marginHorizontal: 10,
    marginTop: 5,
    marginBottom: 10,
  },
});
