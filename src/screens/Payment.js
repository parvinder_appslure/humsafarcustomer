import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {
  BottomButton,
  Header,
  MainView,
  Middleview,
  middleView,
} from '../Custom/CustomView';
import {RadioButton} from 'react-native-paper';
import Dash from 'react-native-dash';
import {TextField} from 'react-native-material-textfield';

const Payment = ({navigation}) => {
  const [checked, setChecked] = useState('first');
  return (
    <ScrollView>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title="Payment" />

      <View style={styles.topView}>
        <Text style={styles.text1_Style}>Payment Details</Text>

        <View style={styles.topv5_Style}>
          <Text style={styles.text2_Style}>MRP Total</Text>
          <Text style={styles.text2_Style}>₹20000.00</Text>
        </View>

        <View style={styles.topv5_Style}>
          <Text style={[styles.text2_Style, {color: '#FF0000'}]}>Discount</Text>
          <Text style={[styles.text2_Style, {color: '#FF0000'}]}>
            -₹1000.00
          </Text>
        </View>

        <View style={styles.topv5_Style}>
          <Text style={styles.text2_Style}>Delivery Charge</Text>
          <Text style={styles.text2_Style}>+ ₹ 500</Text>
        </View>

        <View style={styles.topv5_Style}>
          <Text style={styles.text2_Style}>To Be Paid</Text>
          <Text style={styles.text2_Style}>₹19500.00</Text>
        </View>
        <Dash dashColor="#C8C8D3" style={styles.dash} />

        <View style={styles.topv5_Style}>
          <Text style={styles.text4_Style}>TOTAL AMOUNT</Text>
          <Text style={styles.text4_Style}>₹19500.00/-</Text>
        </View>
      </View>

      <View style={styles.topView}>
        <View style={styles.topv3_Style}>
          <Image
            source={require('../assets/Combined.png')}
            style={styles.img_Style}
          />
          <Text style={styles.text2_Style}>Apply Promo/Referral code</Text>
        </View>

        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderColor: '#00000020',
            borderWidth: 2,
            // width: '90%',
            marginHorizontal: 10,
            borderRadius: 8,
            alignItems: 'center',
            flexDirection: 'row',
            alignSelf: 'center',
            marginTop: 10,
          }}>
          <View style={{width: '75%'}}>
            <TextField
              placeholder={'Enter Coupon Code'}
              fontSize={16}
              textColor={'#000000'}
              tintColor={'#000000'}
              baseColor="#000000"
              labelFontSize={16}
              containerStyle={styles.containerStyle}
              inputContainerStyle={styles.inputContainerStyle}
              lineWidth={0}
              activeLineWidth={0}
            />
          </View>

          <TouchableOpacity
            onPress={() => navigation.navigate('Coupon')}
            style={styles.textfield}>
            <Text style={styles.text2_Style}>Apply</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.topView}>
        <Text style={styles.text1_Style}>Payment Options</Text>
        <View style={styles.line} />

        <View style={styles.topv4_Style}>
          <RadioButton
            value="first"
            status={checked === 'first' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('first')}
            uncheckedColor={'#69707F'}
            color={'#ED6E1E'}
          />
          <Text style={styles.text2_Style}>
            Debit / Credit Card / Netbanking
          </Text>
          <Image
            source={require('../assets/card.png')}
            style={[styles.img_Style, {marginLeft: 'auto'}]}
          />
        </View>
        <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

        <View style={styles.topv4_Style}>
          <RadioButton
            value="Second"
            status={checked === 'Second' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('Second')}
            uncheckedColor={'#69707F'}
            color={'#ED6E1E'}
          />
          <Text style={styles.text2_Style}>Humsafar Wallet</Text>
          <Image
            source={require('../assets/wallet.png')}
            style={[styles.img_Style, {marginLeft: 'auto'}]}
          />
        </View>
        <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

        <View style={styles.topv4_Style}>
          <RadioButton
            value="Third"
            status={checked === 'Third' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('Third')}
            uncheckedColor={'#69707F'}
            color={'#ED6E1E'}
          />
          <Text style={styles.text2_Style}>Cash On Delivery </Text>
          <Image
            source={require('../assets/money.png')}
            style={[styles.img_Style, {marginLeft: 'auto'}]}
          />
        </View>
      </View>

      <View style={{marginHorizontal: 20}}>
        <BottomButton
          onPress={() => navigation.navigate('PaymentSuccessfully')}
          bottomtitle="PROCEED TO PAYMENT"
        />
      </View>
    </ScrollView>
  );
};

export default Payment;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 40,
    elevation: 4,
    paddingBottom: 20,
  },
  dash: {
    marginHorizontal: 20,
    height: 1,
    marginTop: 10,
  },
  topv3_Style: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  img_Style: {
    width: 23,
    height: 23,
    resizeMode: 'contain',
    marginHorizontal: 20,
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginHorizontal: 10,
  },
  textfield: {
    marginHorizontal: 10,
  },
  text1_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#000521',
    fontWeight: 'bold',
    marginHorizontal: 20,
  },
  containerStyle: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    borderColor: '#00000020',
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    height: 48,
  },
  topv5_Style: {
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  line: {
    borderColor: '#00000020',
    borderWidth: 0.5,
    marginTop: 15,
    width: '120%',
    marginLeft: -10,
  },
  text2_Style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    color: '#000521',
    fontWeight: '500',
  },
  text4_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#ED6E1E',
    fontWeight: 'bold',
  },
});
