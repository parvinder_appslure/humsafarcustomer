import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {MainView} from '../Custom/CustomView';
const Cashback = ({navigation}) => {
  const HistoryList = props => (
    <View style={styles.hlBackground}>
      <View style={styles.topList}>
        <View style={styles.line} />
        <View style={{flex: 1}}>
          <View style={styles.nfRow}>
            <Text style={styles.listText}>Paid to Diesel Service</Text>
            <Text style={[styles.listText, {color: '#5DC462'}]}>
              {props.amount}
            </Text>
          </View>

          <View style={styles.nfRow}>
            <Text style={styles.listText_1}>{props.cashback}</Text>
            <Text style={styles.listText_1}>{props.process}</Text>
          </View>

          <View style={styles.nfRow}>
            <Text style={styles.listText_1}>{props.creditDebit}</Text>
            <Text style={styles.listText_1}>{props.SuccessFailed}</Text>
          </View>
        </View>
      </View>
    </View>
  );

  return (
    <MainView>
      <ScrollView>
        <View style={styles.topView}>
          <Text style={styles.topText}>CASHBACK CREDITED</Text>
          <Text style={styles.topText_1}> ₹2500.00</Text>
        </View>
        {/* <HistoryList amount="+ ₹25" date="26 July, 02:19 PM" id="ID: #45683 " /> */}
        <HistoryList
          amount="+ ₹15"
          cashback={' Loyalty Cashback'}
          process="Received "
          creditDebit="Debited : 20 July 2021"
          SuccessFailed="Success "
        />
        <HistoryList
          amount="+ ₹15"
          cashback={' Loyalty Cashback'}
          process="Received "
          creditDebit="Debited : 20 July 2021"
          SuccessFailed="Success "
        />
        <HistoryList
          amount="- ₹250"
          cashback={'Cashback '}
          process="Used  "
          creditDebit="Debited : 20 July 2021"
          SuccessFailed="Success "
        />
        <HistoryList
          amount="- ₹250"
          cashback={'Cashback '}
          process="Used"
          creditDebit="Debited : 20 July 2021"
          SuccessFailed="Success"
        />
      </ScrollView>
    </MainView>
  );
};

export default Cashback;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    margin: 20,
    padding: 20,
    borderRadius: 10,
    marginHorizontal: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },

  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },

  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#000000',
    marginHorizontal: 20,
  },

  hlBackground: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  line: {
    padding: 3,
    backgroundColor: '#ED6E1E',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },

  topList: {
    flexDirection: 'row',
  },

  listText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000521',
  },
  listText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#83878E',
  },
  nfRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
});
