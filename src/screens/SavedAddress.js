import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, Header, MainView} from '../Custom/CustomView';

const SavedAddress = ({navigation}) => {
  const Address = [
    {
      key: 1,
      addressType: 'Home',
      typeSource: require('../assets/home.png'),
      address:
        'C-9/21 Rohini Sector-7 2nd Floor,Oppsite Metro Pillar No.400, Pocket9, Sector 7c, Rohini, Delhi 110085, India',
    },
    {
      key: 2,
      addressType: 'Work',
      typeSource: require('../assets/portfolio.png'),
      address:
        'C-9/21 Rohini Sector-7 2nd Floor,Oppsite Metro Pillar No.400, Pocket9, Sector 7c, Rohini, Delhi 110085, India',
    },
    {
      key: 3,
      addressType: 'Other',
      typeSource: require('../assets/pin.png'),
      address:
        'C-9/21 Rohini Sector-7 2nd Floor,Oppsite Metro Pillar No.400, Pocket9, Sector 7c, Rohini, Delhi 110085, India',
    },
  ];

  return (
    <MainView>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title="Saved Address" />
      <FlatList
        data={Address}
        numColumns={1}
        renderItem={({item}) => {
          return (
            <View style={styles.topView}>
              <Image source={item.typeSource} style={styles.headerImage} />
              <View style={styles.topView1}>
                <Text style={styles.headerText}>{item.addressType}</Text>
                <Text style={styles.headerText1}>{item.address}</Text>
                <View style={styles.topView2}>
                  <Text style={styles.headerText2}>EDIT</Text>
                  <Text style={[styles.headerText2, {marginLeft: 10}]}>
                    DELETE
                  </Text>
                </View>
                <View style={styles.line} />
              </View>
            </View>
          );
        }}
      />
    </MainView>
  );
};

export default SavedAddress;

const styles = StyleSheet.create({
  headerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
  },
  topView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  topView1: {
    marginHorizontal: 20,
  },
  topView2: {
    flexDirection: 'row',
    // margin: 20,
    marginVertical: 10,
  },
  languageSelected: {
    backgroundColor: '#ED6E1E',
    padding: 30,
    borderRadius: 20,
  },
  languageUnselected: {
    backgroundColor: '#00000033',
    padding: 30,
    borderRadius: 20,
  },
  headerImage: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  headerText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#8D92A3',
    marginTop: 5,
  },
  headerText2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#EB2627',
  },
  line: {
    borderColor: '#8D92A320',
    borderWidth: 0.5,
  },
});
