import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
const {width, height} = Dimensions.get('window');

const PaymentSuccessfully = ({navigation}) => {
  return (
    <View style={styles.bg}>
      <StatusBarDark />
      <Image style={styles.image} source={require('../assets/check.png')} />
      <Text style={styles.topText}>Ref Id: 12345</Text>
      <Text style={styles.topText1}>Payment Successful !</Text>
      <Text style={styles.topText2}>
        We are delighted to inform you that we received your payments
      </Text>

      <TouchableOpacity
        onPress={() => navigation.navigate('TopTabNavigation')}
        style={styles.middle}>
        <Text style={styles.topText_1}>Go to My Orders</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PaymentSuccessfully;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: '#31B9EB',
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    marginTop: height / 3,
    alignSelf: 'center',
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 10,
  },
  topText1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    color: '#FFFFFF60',
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 5,
  },
  topText2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#FFFFFF60',
    fontWeight: '500',
    alignSelf: 'center',
    marginTop: 5,
    textAlign: 'center',
    marginHorizontal: 20,
  },
  middle: {
    borderColor: '#FFFFFF60',
    borderWidth: 1,
    borderRadius: 25,
    textAlign: 'center',
    // alignItems: 'center',
    width: width / 2.5,
    alignSelf: 'center',
    padding: 10,
    marginTop: 20,
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#FFFFFF',
    fontWeight: 'bold',
    alignSelf: 'center',
    // marginTop: 5,
  },
});
