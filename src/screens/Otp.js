import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {BottomButton, BottomView, MainImage} from '../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const Otp = ({navigation}) => {
  const [state, setState] = useState({
    otp: '',
  });

  return (
    <KeyboardAwareScrollView>
      <MainImage>
        <BottomView>
          <Text style={styles.topText}>OTP</Text>
          <Text style={styles.topText_1}>
            We will send you confirmation code
          </Text>

          <OTPInputView
            style={styles.otpInput}
            pinCount={4}
            code={state.otp}
            onCodeChanged={code => setState({...state, otp: code})}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            // onCodeFilled={() => navigation.navigate('Professional Details')}
          />
          <BottomButton
            onPress={() => navigation.navigate('Location')}
            bottomtitle="VERIFY"
          />
          <Text style={styles.bottomText}>I did not recieve a code</Text>
          <Text style={styles.bottomText_1}>Resend</Text>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Otp;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F9F9F9',
    backgroundColor: '#F9F9F9',
    borderRadius: 20,
    fontSize: 35,
    color: '#000000',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#ED6E1E',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#E7E7E7',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#100C0850',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
});
