import {DrawerContentScrollView} from '@react-navigation/drawer';
import React, {useMemo} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  View,
  Linking,
  Share,
  StatusBar,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';

const CustomDrawerContent = ({navigation}) => {
  const Data = [
    {
      id: 0,
      title: 'Home',
      value: 'Home',
      source: require('../assets/menu_home.png'),
    },
    {
      id: 1,
      title: 'My Orders',
      value: 'MyOrders',
      source: require('../assets/menu_order.png'),
    },
    {
      id: 2,
      title: 'Change Language',
      value: 'ChangeLanguage',
      source: require('../assets/menu_language.png'),
    },
    {
      id: 3,
      title: 'Saved Address',
      value: 'SavedAddress',
      source: require('../assets/menu_address.png'),
    },

    {
      id: 4,
      title: 'Return Jerry Can',
      value: 'ReturnJerryCan',
      source: require('../assets/menu_return.png'),
    },
    {
      id: 5,
      title: 'My Wallet',
      value: 'MyWallet',
      source: require('../assets/menu_wallet.png'),
    },
    {
      id: 6,
      title: 'Refer & Earn',
      value: 'Refer&Earn',
      source: require('../assets/menu_refer.png'),
    },
    {
      id: 7,
      title: 'Report',
      value: 'Report',
      source: require('../assets/menu_report.png'),
    },
    {
      id: 8,
      title: 'Help',
      value: 'Help',
      source: require('../assets/menu_report.png'),
    },
    {
      id: 9,
      title: 'Faq',
      value: 'Faq',
      source: require('../assets/menu_help.png'),
    },
    {
      id: 10,
      title: 'Privacy Policy',
      value: 'PrivacyPolicy',
      source: require('../assets/menu_privacy.png'),
    },
    {
      id: 11,
      title: 'Terms & Condition',
      value: 'Terms&Condition',
      source: require('../assets/menu_terms.png'),
    },
    {
      id: 12,
      title: 'Logout',
      value: 'Logout',
      source: require('../assets/menu_logout.png'),
    },
  ];

  const onPressHandler = value => {
    console.log(value);
    navigation.closeDrawer();
    switch (value) {
      case 'Home':
        navigation.navigate('home');
        break;
      case 'MyOrders':
        navigation.navigate('TopTabNavigation');
        break;
      case 'ChangeLanguage':
        navigation.navigate('Language');
        break;
      case 'SavedAddress':
        navigation.navigate('SavedAddress');
        break;
      case 'ReturnJerryCan':
        navigation.navigate('ReturnJerryCan');
        break;
      case 'MyWallet':
        navigation.navigate('TopNavigation');
        break;
      case 'Refer&Earn':
        navigation.navigate('BloodDonationHistory');
        break;
      case 'Report':
        navigation.navigate('DonationHistory');
        break;
      case 'Help':
        navigation.navigate('HelpDesk');
        break;
      case 'Faq':
        navigation.navigate('Settings');
        break;
      case 'PrivacyPolicy':
        navigation.navigate('Settings');

        break;
      case 'Terms&Condition':
        navigation.navigate('Settings');
        break;
      case 'Logout':
        navigation.navigate('Settings');
        break;

      default:
    }
  };

  const optionView = ({source, title, value}) => (
    <TouchableOpacity
      style={styles.controlView_2}
      onPress={() => onPressHandler(value)}>
      <Image source={source} style={styles.constrolViewImage} />
      <Text style={styles.controlViewText}>{title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#31B9EB'}}>
      <DrawerContentScrollView>
        <View style={styles.controlView}>
          <Text style={styles.dnTopText}>Rahul Malhotra</Text>
          <Text style={styles.dnTopText1}>+91 9599499793</Text>
          <Text style={[styles.dnTopText1, {marginBottom: 10}]}>
            View Profile
          </Text>
          {Data.map(item => (
            <>{optionView(item)}</>
          ))}
          <Text style={styles.appVersion}>App version 1.0</Text>
        </View>
      </DrawerContentScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  controlView: {
    flex: 1,
    backgroundColor: '#31B9EB',
    width: '100%',
    paddingTop: 10,
  },
  appVersion: {
    marginTop: 'auto',
    marginBottom: 8,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#FFFFFF',
    opacity: 0.5,
    alignSelf: 'center',
  },
  controlView_2: {
    marginHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  constrolViewImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  controlViewText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    marginLeft: '4%',
  },
  dnTopText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFFFFF',
    // marginTop: 10,
    marginHorizontal: 30,
  },
  dnTopText1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '600',
    fontSize: 14,
    color: '#FFFFFF',
    marginHorizontal: 30,
    marginTop: 5,
  },
});
export default CustomDrawerContent;
