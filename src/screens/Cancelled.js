import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Cancelled = () => {
  return (
    <View style={{backgroundColor: 'yellow', flex: 1}}>
      <Text>hellow Cancelled</Text>
    </View>
  );
};

export default Cancelled;

const styles = StyleSheet.create({});
