import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Dash from 'react-native-dash';
import CustomModal from '../Custom/CustomModal';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {ButtonStyle} from '../Custom/CustomView';

const {height} = Dimensions.get('window');

const Upcoming = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalState, setModalState] = useState({
    visible: false,
  });
  const person = [
    {
      key: 1,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 2,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 3,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 4,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 5,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
    {
      key: 6,
      order: 'Order #123456',
      date: '25/07/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: ' Ongoing',
      name: 'Deepak Sharma',
      address: 'C-9/21 Rohini Sector-7',
      dateTime: '26 July 21, 11:00 AM',
      dieselPrice: '89.91/litre',
      jerryCan: '40 ltr',
      paymenyMethod: 'Cash',
      otp: '5678',
      total: ' ₹19500/- ',
    },
  ];
  return (
    <View>
      <StatusBarLight />
      <FlatList
        data={person}
        numColumns={1}
        renderItem={({item}) => {
          return (
            <>
              <View style={styles.bg}>
                <View style={styles.ftview}>
                  <Text style={styles.ftText}>{item.order}</Text>
                  <Text style={styles.ftText1}>{item.date}</Text>
                </View>

                <DashLine />

                <Text style={styles.ftText2}>
                  Status:
                  <Text style={[styles.ftText2, {color: '#FF9600'}]}>
                    {item.ongoing}
                  </Text>
                </Text>

                <View style={styles.ftview_1}>
                  <View>
                    <Text style={styles.ftText_2}>Name</Text>
                    <Text style={styles.ftText_2}>Address</Text>
                    <Text style={styles.ftText_2}>Date & Time</Text>
                    <Text style={styles.ftText_2}> Diesel Price</Text>
                    <Text style={styles.ftText_2}>Jerry Can</Text>
                    <Text style={styles.ftText_2}>Payment Mode</Text>
                    <Text style={styles.ftText_2}>OTP</Text>
                  </View>

                  <View>
                    <Text style={styles.ftText_3}>{item.name}</Text>
                    <Text style={styles.ftText_3}>{item.address}</Text>
                    <Text style={styles.ftText_3}>{item.dateTime}</Text>
                    <Text style={styles.ftText_3}>{item.dieselPrice}</Text>
                    <Text style={styles.ftText_3}>{item.jerryCan}</Text>
                    <Text style={styles.ftText_3}>{item.paymenyMethod}</Text>
                    <Text style={[styles.ftText_3, {color: '#ED6E1E'}]}>
                      {item.otp}
                    </Text>
                  </View>
                </View>

                <DashLine />
                <View style={[styles.ftview_1, {marginTop: 0}]}>
                  <Text style={styles.ftText}>
                    Total:
                    <Text style={[styles.total, {color: '#F87B00'}]}>
                      {item.total}
                    </Text>
                  </Text>

                  {ButtonStyle('PAY NOW ', '#ED6E1E', '#FFFFFF', () =>
                    navigation.navigate('Payment'),
                  )}
                </View>

                <DashLine />

                <View style={styles.bottomView}>
                  {ButtonStyle('TRACK ORDER ', '#ED6E1E', '#FFFFFF', () =>
                    navigation.navigate(''),
                  )}

                  {ButtonStyle('CANCEL', '#FF0000', '#FFFFFF', () =>
                    setModalState(true),
                  )}

                  <CustomModal visible={modalState.visible}>
                    <View style={styles.madal_Style}>
                      <Image
                        source={require('../assets/remove.png')}
                        style={styles.mdImage}
                      />
                      <Text style={styles.mdText}>Cancel Order</Text>
                      <Text style={styles.mdText_1}>
                        are you sure you want to cancel this order?
                      </Text>
                      <View style={styles.mdView}>
                        <TouchableOpacity
                          onPress={() => navigation.navigate('')}>
                          <Text style={[styles.mdText, {color: '#FF0000'}]}>
                            YES
                          </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => {
                            setModalState({visible: false});
                            navigation.navigate('Upcoming');
                          }}>
                          <Text
                            style={[
                              styles.mdText,
                              {color: '#ED6E1E', marginHorizontal: 20},
                            ]}>
                            NO
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </CustomModal>
                </View>
              </View>
            </>
          );
        }}
      />
    </View>
  );
};

export default Upcoming;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 10,
    top: 10,
    borderRadius: 10,
    marginHorizontal: 20,
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#9F9F9F',
  },
  dash: {
    borderColor: '#6F6F7B',
    marginHorizontal: 20,
    borderStyle: 'dotted',
    borderWidth: 1,
    marginTop: 10,
  },
  ftText2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#9F9F9F',
    marginHorizontal: 10,
  },

  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
  },
  ftText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 3,
  },
  ftText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#83878E',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#000000',
    alignSelf: 'flex-end',
  },
  ftText_4: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#000000',
    alignSelf: 'flex-end',
  },
  payButton: {
    padding: 5,
    borderRadius: 4,
    backgroundColor: '#ED6E1E',
  },
  payText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    margin: 10,
  },

  madal_View: {
    flex: 1,
    backgroundColor: '#00000026',
    // backgroundColor: '#7d7a821a',
  },
  madal_Style: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: -70,
    borderRadius: 20,
    marginHorizontal: 20,
  },
  mdImage: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  mdText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
    alignSelf: 'center',
    marginTop: 10,
  },
  mdText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    fontWeight: '500',
    color: '#1e1f2080',
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: 20,
  },
  mdView: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginHorizontal: 10,
    marginBottom: 10,
    // justifyContent: 'space-around',
  },
});

export const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);

const ModalView = props => (
  <Modal visible={modalOpen} transparent={true} {...props}></Modal>
);
