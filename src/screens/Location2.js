import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Modal,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, MainView} from '../Custom/CustomView';
// import RadioForm, {
//   RadioButton,
//   RadioButtonInput,
//   RadioButtonLabel,
// } from 'react-native-simple-radio-button';
const {height, width} = Dimensions.get('window');

const Location2 = ({navigation}) => {
  //   radio_props = [{label: 'param2'}];
  const [state, setstate] = useState({
    value: 0,
  });

  const radioProps = [
    {
      value: false,
    },
  ];

  const [modalOpen, setModalOpen] = useState(false);

  const onChangeValue = (item, index) => {
    const data = person.map(newItem => {
      if (newItem.id == item.id) {
        return {
          ...newItem,
          // value=true
        };
      }

      return {
        ...newItem,
        value: false,
      };
    });
    setstate(data);
  };

  return (
    <ScrollView>
      <MainView>
        <StatusBarDark />
        <View style={styles.topView}>
          <View style={styles.header}>
            <Image
              source={require('../assets/location.png')}
              style={styles.headerImage}
            />
            <Text style={styles.headerText}>Pearls Omaxe, Netaji…</Text>
          </View>
        </View>

        <View style={styles.map}></View>

        <View style={styles.orderDetails}>
          <View style={styles.middleStyle}>
            <Text style={styles.middleText}>Jerry Can</Text>
            <Text style={styles.middleText_1}>Order Id: 4567</Text>
          </View>
          <View style={styles.middleStyle}>
            <Text style={styles.middleText_1}>20 ltr</Text>
            <TouchableOpacity onPress={() => navigation.navigate('MyDrawer')}>
              <Text style={styles.middleText_3}>Order Again</Text>
            </TouchableOpacity>
          </View>
        </View>

        <FlatList
          data={person}
          numColumns={1}
          //   columnWrapperStyle={{
          //     justifyContent: 'space-between',
          //     marginVertical: 5,
          //     marginHorizontal: 20,
          //   }}
          renderItem={({item}) => {
            return (
              <View>
                <View style={styles.ftview}>
                  <Image source={item.homesource} style={styles.ftImage} />
                  <Text style={styles.ftTitle}>{item.title}</Text>
                </View>
                <View style={{marginLeft: 20}}>
                  <RadioForm
                    buttonColor={'#69707F'}
                    buttonSize={12}
                    radioStyle={{paddingTop: 25}}
                    selectedButtonColor="#ED6E1E"
                    radio_props={radioProps}
                    initial={0}
                    animation={false}
                    onPress={() => onChangeValue(item, index)}
                    // onValueChange
                  />
                </View>
                <View style={styles.ftView1}>
                  <Text style={styles.ftText}>EDIT</Text>
                  <Text style={styles.ftText}>DELETE</Text>
                </View>
              </View>
            );
          }}
        />

        <View style={styles.bottomView}>
          <Text style={styles.bottomText}>ADD NEW ADDRESS</Text>
        </View>
        <BottomButton
          onPress={() => setModalOpen(true)}
          bottomtitle="CONTINUE"
        />
        <Modal visible={modalOpen} transparent={true}>
          <View style={styles.modal_View}>
            <View style={styles.mdtop}>
              <View style={styles.mdtop_1}>
                <Text style={styles.mdTopText}>Delivery Options:</Text>
              </View>
              <View style={styles.mdmiddleView}>
                <View style={styles.mdmiddle}>
                  <Image
                    source={require('../assets/truck.png')}
                    style={styles.mdImage}
                  />
                  <Text style={styles.mdText}>Browser</Text>
                </View>

                <View style={styles.mdmiddle_1}>
                  <Image
                    source={require('../assets/can.png')}
                    style={styles.mdImage_1}
                  />
                  <Text style={styles.mdText}>Safar 20 (Jerry Can)</Text>
                </View>
              </View>

              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.mdbottomView}
                onPress={() => {
                  navigation.navigate('');
                  setModalOpen(false);
                }}>
                <Text style={styles.mdBottomText}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </MainView>
    </ScrollView>
  );
};

export default Location2;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    padding: 10,
  },
  headerText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 10,
  },
  headerImage: {
    width: 12,
    height: 18,
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 20,
  },
  map: {
    backgroundColor: 'grey',
    height: 340,
  },
  orderDetails: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    elevation: 5,
    marginTop: 30,
  },
  middleStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: 5,
  },
  middleText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1D1E2C',
  },
  middleText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#83878E',
  },
  middleText_3: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#F87B00',
    textAlign: 'right',
  },

  ftview: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
    marginTop: 30,
  },
  ftTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  ftImage: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  bottomView: {
    borderWidth: 1,
    borderColor: '#ED6E1E',
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 5,
    marginTop: 20,
  },
  bottomText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
  },
  ftView1: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 50,
    marginTop: 10,
  },
  ftText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#EB2627',
    marginHorizontal: 10,
  },
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#31B9EB',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  mdmiddle: {
    backgroundColor: '#ED6E1E',
    padding: 10,
    width: width / 3,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage: {
    width: 95,
    height: 42,
    resizeMode: 'contain',
    marginTop: 10,
  },
  mdText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 5,
  },
  mdmiddle_1: {
    backgroundColor: '#00000030',
    padding: 10,
    width: width / 2.5,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage_1: {
    width: 40,
    height: 48,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 10,
  },
  mdmiddleView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 15,
  },
  mdbottomView: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

const person = [
  {
    key: 1,
    homesource: require('../assets/home.png'),
    title: 'Home',
    address:
      'C-9/21 Rohini Sector-7 2nd Floor,Oppsite Metro Pillar No.400, Pocket9, Sector 7c, Rohini, Delhi 110085, India',
    value: 0,
  },
  {
    key: 2,
    homesource: require('../assets/portfolio.png'),
    title: 'Work',
    address:
      'C-9/21 Rohini Sector-7 2nd Floor,Oppsite Metro Pillar No.400, Pocket9, Sector 7c, Rohini, Delhi 110085, India',
    value: 1,
  },
];
