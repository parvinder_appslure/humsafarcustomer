import React, {useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {BottomButton, CustomTextField} from '../Custom/CustomView';
import {Dropdown} from 'react-native-material-dropdown-v2';
import {TextField} from 'react-native-material-textfield';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

const {width, height} = Dimensions.get('window');
const home = ({navigation}) => {
  const fuelTypeRef = useRef();
  const machineTypeRef = useRef();
  const fuelTypeList = [
    {
      id: '1',
      value: 'Diesel',
    },
    // {
    //   id: '2',
    //   value: 'Female',
    // },
  ];

  const machineTypeList = [
    {
      id: '1',
      value: 'Diesel Genset',
    },
    {
      id: '2',
      value: 'Barrel',
    },
    {
      id: '3',
      value: 'Earth Moving Equipment`s',
    },
    {
      id: '4',
      value: 'Tunnel Boring Machines',
    },
    {
      id: '5',
      value: 'Others',
    },
  ];

  const [state, setState] = useState({
    fuelType: '',
    machineType: '',
    price: '',
    quantity: '',
    showPicker: false,
    date: '',
    dob: '',
  });

  const [timeState, setTimeState] = useState({
    time: new Date(),
    show: false,
  });

  const showDateTimePicker = () => (
    <DateTimePicker
      value={state.date || new Date()}
      maximumDate={new Date()}
      mode={'date'}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          setState({
            ...state,
            showPicker: false,
            dob: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
            date: nativeEvent.timestamp,
          });
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );

  return (
    <ScrollView>
      <StatusBarDark />
      <View style={styles.topView}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.navigate('MyDrawer')}>
            <Image
              source={require('../assets/menu.png')}
              style={styles.headerImage}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Image
              source={require('../assets/notification.png')}
              style={styles.headerImage_1}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Text style={styles.headerText}>Order Detail</Text>

      <Text style={styles.headerText_1}>Select Fuel Type</Text>

      <Dropdown
        ref={fuelTypeRef}
        rippleOpacity={0.54}
        dropdownPosition={-2}
        onChangeText={fuelType => setState({...state, fuelType})}
        data={fuelTypeList}
        labelExtractor={item => item.value}
        valueExtractor={item => item.value}
        containerStyle={{height: 0, width: 0}}
        pickerStyle={{width: width - 40, left: 20}}
      />
      <TouchableOpacity onPress={() => fuelTypeRef.current.focus()}>
        <CustomTextField
          // label={'Blood Group'}
          defaultValue={state.fuelType}
          onChangeText={fuelType => setState({...state, fuelType})}
          disabled={true}
        />
      </TouchableOpacity>
      <Text style={styles.headerText_1}>Select Machine Type</Text>
      <Dropdown
        ref={machineTypeRef}
        rippleOpacity={0.54}
        dropdownPosition={-2}
        onChangeText={machineType => setState({...state, machineType})}
        data={machineTypeList}
        labelExtractor={item => item.value}
        valueExtractor={item => item.value}
        containerStyle={{height: 0, width: 0}}
        pickerStyle={{width: width - 40, left: 20}}
      />
      <TouchableOpacity onPress={() => machineTypeRef.current.focus()}>
        <CustomTextField
          // label={'Blood Group'}
          defaultValue={state.machineType}
          onChangeText={machineType => setState({...state, machineType})}
          disabled={true}
        />
      </TouchableOpacity>

      <Text style={styles.headerText_1}>Price</Text>
      <CustomTextField
        defaultValue={state.price}
        onChangeText={price => setState({...state, price})}
      />

      <Text style={styles.headerText_1}>Quantity (in litres)</Text>
      <CustomTextField
        defaultValue={state.quantity}
        onChangeText={quantity => setState({...state, quantity})}
      />

      <View style={styles.bottom1}>
        <Text style={styles.headerText_1}>Date</Text>
        <Text style={[styles.headerText_2, {marginLeft: 100}]}>Time</Text>
      </View>

      <View style={[styles.bottom, {marginHorizontal: 25}]}>
        <TouchableOpacity
          style={styles.date}
          onPress={() => setState({...state, showPicker: true})}>
          <TextField
            fontSize={18}
            defaultValue={state.dob}
            textColor={'#1E2432'}
            tintColor={'grey'}
            disabled={true}
            containerStyle={{
              backgroundColor: '#FFFFFF',
              width: '80%',
              borderRadius: 10,
            }}
            inputContainerStyle={{marginHorizontal: 10, height: 48}}
          />
          <Image
            source={require('../assets/calendar.png')}
            style={styles.headerImage}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.date}
          onPress={() => setTimeState({...timeState, show: true})}
          // onPress={() => setState({...state, showPicker: true})}
        >
          <TextField
            fontSize={18}
            textColor={'#1E2432'}
            tintColor={'grey'}
            containerStyle={{
              backgroundColor: '#FFFFFF',
              width: '80%',
              borderRadius: 10,
            }}
            inputContainerStyle={{marginHorizontal: 20, height: 48}}
            disabled={true}
            defaultValue={moment(timeState.time).format('hh:mm a')}
          />

          {timeState.show && (
            <DateTimePicker
              testID="dateTimePicker"
              minimumDate={new Date()}
              value={timeState.time}
              mode={'time'}
              // is24Hour={true}
              display="default"
              onChange={event => {
                if (event.type === 'set') {
                  setTimeState({
                    time: new Date(event.nativeEvent.timestamp),
                    show: false,
                  });
                } else {
                  setTimeState({
                    ...timeState,
                    show: false,
                  });
                }
              }}
            />
          )}

          <Image
            source={require('../assets/clock.png')}
            style={styles.headerImage}
          />
        </TouchableOpacity>
      </View>

      <Text style={styles.dateText}>
        Note:
        <Text style={{color: '#2D2627'}}>
          {` You will be billed/invoiced based on daily rate applicable on the date of delivery of fuel.`}
        </Text>
      </Text>

      <View style={{marginHorizontal: 20}}>
        <BottomButton
          onPress={() => navigation.navigate('SelectVender')}
          bottomtitle="CONTINUE"
        />
      </View>
      {state.showPicker && showDateTimePicker()}
    </ScrollView>
  );
};

export default home;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    padding: 10,
  },
  headerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
    alignSelf: 'center',
    marginTop: 20,
  },
  headerImage: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  headerImage_1: {
    width: 18,
    height: 20,
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 10,
    justifyContent: 'space-between',
  },

  dateText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#FF0000',
    margin: 30,
  },
  headerText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 5,
    marginHorizontal: 30,
  },
  headerText_2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 5,
  },
  inputfield: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
    elevation: 5,
  },
  containerStyle: {},
  labelTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    fontSize: 16,
    color: '#ACB1C0',
  },
  bottom: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 30,
  },
  bottom1: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between',
    marginTop: 10,
    // marginHorizontal: 30,
  },
  date: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    marginTop: 10,
    width: width / 2.5,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
