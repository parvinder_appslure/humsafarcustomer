import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';

const {height} = Dimensions.get('window');

export const MainView = props => (
  <SafeAreaView style={{backgroundColor: '#FFFFFF', flex: 1}} {...props} />
);

export const MainImage = props => (
  <ImageBackground
    source={require('../assets/background.png')}
    style={mainStyle.bg}
    {...props}></ImageBackground>
);
const mainStyle = StyleSheet.create({
  bg: {
    flex: 1,
  },
  BottomView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 470,
    paddingTop: 20,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
  },
});
export const BottomView = props => (
  <View style={mainStyle.BottomView} {...props} />
);

export const ButtonStyle = (
  title,
  bgColor = '#ED6E1E',
  txtcolor = '#FFFFFF',
  onPress,
) => (
  <TouchableOpacity
    activeOpacity={0.6}
    onPress={onPress}
    style={[styles.facebookButton, {backgroundColor: bgColor}]}>
    <Text style={[styles.facebooktext, {color: txtcolor}]}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  facebookButton: {
    backgroundColor: '#3B5998',
    padding: 8,
    borderRadius: 5,
    marginLeft: 15,
  },
  facebooktext: {
    fontFamily: 'Avenir-heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  containerStyle: {
    width: '90%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 40,
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    backgroundColor: '#1B172C',
  },
});

export const Header = props => (
  <View style={headerStyle.viewHeader}>
    <View style={headerStyle.flexView}>
      <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
        <Image
          source={require('../assets/back.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </View>
);

const headerStyle = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#31B9EB',
    padding: 10,
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const BottomButton = props => (
  <TouchableOpacity
    activeOpacity={0.8}
    style={bottomStyle.bottomView}
    onPress={props.onPress}>
    <Text style={bottomStyle.textTitle}>{props.bottomtitle}</Text>
  </TouchableOpacity>
);

export const bottomStyle = StyleSheet.create({
  bottomView: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 'auto',
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

export const CustomTextField = props => (
  <TextField
    fontSize={18}
    textColor={'#1E2432'}
    tintColor={'grey'}
    containerStyle={{
      backgroundColor: '#FFFFFF',
      marginTop: 20,
      marginHorizontal: 20,
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      marginBottom: 10,
      elevation: 4,
    }}
    inputContainerStyle={{marginHorizontal: 20, height: 48}}
    {...props}
  />
);
